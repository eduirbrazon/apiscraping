package share

import "github.com/gofiber/fiber/v2"

func Response(c *fiber.Ctx, i interface{}, err error) error {
	if err != nil {
		return c.Status(err.(*fiber.Error).Code).JSON(fiber.Map{
			"error":   "Error en la consulta",
			"message": err.Error(),
			"data":    nil,
		})
	}
	return c.Status(200).JSON(fiber.Map{
		"error":   false,
		"message": "OK",
		"data":    i,
	})
}
