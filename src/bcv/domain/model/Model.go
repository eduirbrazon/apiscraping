package model

type Exchange struct {
	Name  string  `json:"name"`
	Value float32 `json:"value"`
}
