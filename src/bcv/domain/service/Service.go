package service

import (
	"bcvscrap/src/bcv/domain/model"
	"strconv"
	"strings"

	"github.com/gocolly/colly"
)

func GetRates() (model.Exchange, error) {
	var errs error
	exchange := model.Exchange{
		Name: "usd",
	}
	c := colly.NewCollector()
	c.OnHTML("#dolar", func(h *colly.HTMLElement) {
		value := h.ChildText(".centrado strong")
		value = strings.Replace(value, ",", ".", -1)
		f32, err := strconv.ParseFloat(value, 32)
		if err != nil {
			errs = err
		}
		exchange.Value = float32(f32)
	})
	c.Visit("https://www.bcv.org.ve")
	if errs != nil {
		return exchange, errs
	}

	return exchange, nil
}
