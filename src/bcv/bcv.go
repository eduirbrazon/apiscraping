package bcv

import (
	"bcvscrap/src/bcv/app"

	"github.com/gofiber/fiber/v2"
)

func Module(fapp *fiber.App) {
	exchange := fapp.Group("/exch")
	app.BcvRoutes(exchange)
}
