package app

import (
	"bcvscrap/src/bcv/domain/service"
	"bcvscrap/src/share"

	"github.com/gofiber/fiber/v2"
)

func GetUSD(c *fiber.Ctx) error {
	exchange, err := service.GetRates()
	return share.Response(c, exchange, err)
}
