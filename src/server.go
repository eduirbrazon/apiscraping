package src

import (
	"bcvscrap/src/bcv"

	"github.com/gofiber/fiber/v2"
)

// func init() {
// 	err := godotenv.Load()
// 	if err != nil {
// 		panic(err)
// 	}
// }

func Runserver() {
	app := fiber.New()
	bcv.Module(app)

	// app.Listen(os.Getenv("PORT"))
	app.Listen(":8005")
}
